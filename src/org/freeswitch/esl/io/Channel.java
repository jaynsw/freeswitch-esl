package org.freeswitch.esl.io;

import java.util.HashMap;
import java.util.Map;

import org.freeswitch.esl.domain.Command;
import org.freeswitch.esl.domain.CommandResponse;
import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.SendMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Channel {
	static final Logger logger = LoggerFactory.getLogger(Channel.class);

	public static final String INBOUND = "inbound";
	public static final String OUTBOUND = "outbound";

	private String channelId;

	private IEngine engine;

	private String callDirection;

	private ISession session;

	private Map attributes = new HashMap();

	private String from;

	private String to;

	public Map getAttributes() {
		return attributes;
	}

	public enum State {
		INIT, ACTIVE, DEAD;
	}

	private State state = State.INIT;

	public void setState(State state) {
		this.state = state;
	}

	public State getState() {
		return state;
	}

	/**
	 * sets the uuid from this channel. be careful !!
	 * 
	 * @param uuid
	 */
	public void setChannelId(String uuid) {
		this.channelId = uuid;
	}

	/**
	 * 
	 * @return the channel uuid
	 */
	public String getChannelId() {
		return channelId;
	}

	
	
	public Channel(String channelId, IEngine engine,
			String callDirection) {
		this.channelId = channelId;
		this.engine = engine;
		this.callDirection = callDirection;
	}
	
	

	/**
	 * executes a freeswitch application on this channel
	 * 
	 * @param appName
	 *            freeswitch application name
	 * @param appArgs
	 *            arguments of this freeswitch application
	 */
	public CommandResponse executeApplication(final long delaymilliseconds,
			final String appName, final String appArgs) {
		if (state != State.ACTIVE) {
			logger.warn("channel " + channelId
					+ " has state {}, application ignored: {}", state, appName);
			throw new IllegalStateException("The channel state " + state
					+ " is illeage ");
		}
		SendMsg msg = new SendMsg(channelId);
		msg.addCallCommand(SendMsg.EXECUTE);
		msg.addExecuteAppName(appName);
		msg.addExecuteAppArg(appArgs);

		return engine.asyncMessage(delaymilliseconds, msg);
	}

	public CommandResponse delayEvent(final long delayMilliSeconds,
			String subClass) {
		return executeApplication(delayMilliSeconds, "event", "Event-Subclass="
				+ subClass);
	}

	public CommandResponse hangup(final long delaymilliseconds,
			final int hangupCause) {
		if (state != State.ACTIVE) {

			logger.warn("channel " + channelId
					+ " has state {}, hangupCause: {}", state, hangupCause);
			throw new IllegalStateException("The channel state " + state
					+ " is illeage ");
		}
		SendMsg msg = new SendMsg(channelId);
		msg.addCallCommand(SendMsg.HANGUP);
		msg.addHangupCause("" + hangupCause);

		return engine.asyncMessage(delaymilliseconds, msg);
	}

	public CommandResponse uuidCommand(final long delaymilliseconds,
			final String cmd, final String args) {
		if (state != State.ACTIVE) {

			logger.warn("channel " + channelId
					+ " has state {}, cmd ignored: {}", state, cmd);
			throw new IllegalStateException("The channel state " + state
					+ " is illeage ");
		}
		String argss = channelId + " " + args;
		Command command = new Command(Command.BGAPI, cmd, argss);
		return engine.runCommand(delaymilliseconds, command);
	}

	@Override
	public String toString() {
		return "Channel [id=" + channelId + ", state=" + state + ",direction="
				+ callDirection + "]";
	}

	/**
	 * This method will be invoked each time the {@link FSController} receives
	 * event for this channel.
	 * 
	 * @param event
	 */
	void onEvent(Event event) {
		session.onEvent(this, event);
	}

	public String getCallDirection() {
		return callDirection;
	}

	public ISession getSession() {
		return session;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public void setSession(ISession session){
		this.session = session;
	}

}
