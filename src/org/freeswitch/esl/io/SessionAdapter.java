package org.freeswitch.esl.io;

import org.freeswitch.esl.domain.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionAdapter implements ISessionListener{

	protected final Logger log = LoggerFactory.getLogger( this.getClass() );
	
	@Override
	public void onEventChannelAnswer(Channel channel, Event event) {
		String fun = "onEventChannelAnswer";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelApplication(Channel channel, Event event) {
		String fun = "onEventChannelApplication";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelBridge(Channel channel, Event event) {
		String fun = "onEventChannelBridge";
		log(fun, channel, event);
		
	}

	@Override
	public void onEventChannelCallState(Channel channel, Event event) {
		String fun = "onEventChannelCallState";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelCreate(Channel channel, Event event) {
		String fun = "onEventChannelCreate";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelData(Channel channel, Event event) {
		String fun = "onEventChannelData";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelDestroy(Channel channel, Event event) {
		String fun = "onEventChannelDestroy";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelExecute(Channel channel, Event event) {
		String fun = "onEventChannelExecute";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelExecuteComplete(Channel channel, Event event) {
		String fun = "onEventChannelExecuteComplete";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelHangup(Channel channel, Event event) {
		String fun = "onEventChannelHangup";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelHangupComplete(Channel channel, Event event) {
		String fun = "onEventChannelHangupComplete";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelHold(Channel channel, Event event) {
		String fun = "onEventChannelHold";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelOriginate(Channel channel, Event event) {
		String fun = "onEventChannelOriginate";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelOutgoing(Channel channel, Event event) {
		String fun = "onEventChannelOutgoing";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelPark(Channel channel, Event event) {
		String fun = "onEventChannelPark";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelProgress(Channel channel, Event event) {
		String fun = "onEventChannelProgress";
		log(fun, channel, event);
	}
	

	@Override
	public void onEventChannelProgressMedia(Channel channel, Event event) {
		String fun = "onEventChannelProgressMedia";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelState(Channel channel, Event event) {
		String fun = "onEventChannelState";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelUnBridge(Channel channel, Event event) {
		String fun = "onEventChannelUnBridge";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelUnHold(Channel channel, Event event) {
		String fun = "onEventChannelUnHold";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelUnPark(Channel channel, Event event) {
		String fun = "onEventChannelUnPark";
		log(fun, channel, event);
	}

	@Override
	public void onEventChannelUUID(Channel channel, Event event) {
		String fun = "onEventChannelUUID";
		log(fun, channel, event);
	}

	@Override
	public void onEventCustom(Channel channel, Event event) {
		String fun = "onEventCustom";
		log(fun, channel, event);
	}

	@Override
	public void onEventDTMF(Channel channel, Event event) {
		String fun = "onEventDTMF";
		log(fun, channel, event);
	}

	@Override
	public void onEventPlaybackStart(Channel channel, Event event) {
		String fun = "onEventPlaybackStart";
		log(fun, channel, event);
	}

	@Override
	public void onEventPlaybackStop(Channel channel, Event event) {
		String fun = "onEventPlaybackStop";
		log(fun, channel, event);
	}

	@Override
	public void onEventPrivateCommand(Channel channel, Event event) {
		String fun = "onEventPrivateCommand";
		log(fun, channel, event);
	}

	@Override
	public void onEventRecordStart(Channel channel, Event event) {
		String fun = "onEventRecordStart";
		log(fun, channel, event);
	}

	@Override
	public void onEventRecordStop(Channel channel, Event event) {
		String fun = "onEventRecordStop";
		log(fun, channel, event);
	}
	
	public void onEventPresenceIn(Channel channel, Event event){
		String fun = "onEventPresenceIn";
		log(fun, channel, event);
	}
	
	void log(String fun, Channel channel, Event event){
		String channelId = event.getChannelId();
		String name = event.getEventName();
		log.info("sessionadapter->{} channelId:{} event:{}", fun, channelId, name);
	}

}
