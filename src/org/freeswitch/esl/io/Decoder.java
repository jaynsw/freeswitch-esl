package org.freeswitch.esl.io;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.handler.codec.TooLongFrameException;

import java.util.List;

import org.freeswitch.esl.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Decoder extends ReplayingDecoder<Decoder.State> {

	   /**
     * Line feed character
     */
    static final byte LF = 10;

    protected static enum State
    {
        READ_HEADER,
        READ_BODY,
    }
    
    private final Logger log = LoggerFactory.getLogger( this.getClass() );
    private final int maxHeaderSize;
    private Message currentMessage;
   
    public Decoder( int maxHeaderSize )
    {
        super( State.READ_HEADER );
        if (maxHeaderSize <= 0) 
        {
            throw new IllegalArgumentException(
                    "maxHeaderSize must be a positive integer: " +
                    maxHeaderSize);
        }
        this.maxHeaderSize = maxHeaderSize;
    }
    
    
    
    @Override
    protected void decode( ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception
    {
    	State state = state();
        log.trace( "decode() : state [{}]", state );
        switch ( state )
        {
        case READ_HEADER:
            if ( currentMessage == null )
            {
                currentMessage = new Message();
            }
            /*
             *  read '\n' terminated lines until reach a single '\n'
             */
            boolean reachedDoubleLF = false;
            while ( ! reachedDoubleLF )
            {
                // this will read or fail
                String headerLine = readToLineFeedOrFail( in, maxHeaderSize );
                log.debug( "read header line [{}]", headerLine );
                if ( ! headerLine.isEmpty() )
                {
                    // split the header line
                    String[] headerParts = HeaderParser.splitHeader( headerLine );
                    
                    currentMessage.addHeader( headerParts[0], headerParts[1] );
                }
                else
                {
                    reachedDoubleLF = true;
                }
                // do not read in this line again
                checkpoint();
            } 
            // have read all headers - check for content-length
            if ( currentMessage.hasContentLength() )
            {
                checkpoint( State.READ_BODY );
                log.debug( "have content-length, decoding body .." );
                //  force the next section

                return;
            }
            else
            {
                // end of message
                checkpoint( State.READ_HEADER );
                // send message upstream
                Message decodedMessage = currentMessage;
                currentMessage = null;
                
                out.add(decodedMessage);
                break;
            }
            

        case READ_BODY:
            /*
             *   read the content-length specified
             */
            int contentLength = currentMessage.getContentLength();
            ByteBuf bodyBytes = in.readBytes( contentLength );
            log.debug( "read [{}] body bytes", bodyBytes.writerIndex() );
            // most bodies are line based, so split on LF
            while( bodyBytes.isReadable() )
            {
                String bodyLine = readLine( bodyBytes, contentLength );
                log.debug( "read body line [{}]", bodyLine );
                currentMessage.addBodyLine( bodyLine );
            }
            
            // end of message
            checkpoint( State.READ_HEADER );
            // send message upstream
            Message decodedMessage = currentMessage;
            currentMessage = null;
            
            out.add(decodedMessage);
            break;
        default:
            throw new Error( "Illegal state: [" + state  + ']' );
        }
    }

    private String readToLineFeedOrFail( ByteBuf buffer, int maxLineLegth ) throws TooLongFrameException
    {
        StringBuilder sb = new StringBuilder(64);
        while ( true ) 
        {
            // this read might fail
            byte nextByte = buffer.readByte();
            if ( nextByte == LF ) 
            {
                return sb.toString();
            }
            else
            {
                // Abort decoding if the decoded line is too large.
                if ( sb.length() >=  maxLineLegth ) 
                {
                    throw new TooLongFrameException(
                            "ESL header line is longer than " + maxLineLegth + " bytes.");
                }
                sb.append( (char) nextByte );
            }
        }
    }
   
    private String readLine( ByteBuf buffer, int maxLineLength ) throws TooLongFrameException 
    {
        StringBuilder sb = new StringBuilder(64);
        while ( buffer.isReadable() ) 
        {
            // this read should always succeed
            byte nextByte = buffer.readByte();
            if (nextByte == LF) 
            {
                return sb.toString();
            }
            else 
            {
                // Abort decoding if the decoded line is too large.
                if ( sb.length() >= maxLineLength ) 
                {
                    throw new TooLongFrameException(
                            "ESL message line is longer than " + maxLineLength + " bytes.");
                }
                sb.append( (char) nextByte );
            }
        }
        
        return sb.toString();
    }
}
