package org.freeswitch.esl.io;

import java.io.Writer;

import org.freeswitch.esl.domain.Event;

public interface ISession {
	String SESSION_ID = "ESL_SESSION_ID";
	String getSessionId();
	void setSessionId(String id);
	void onEvent(Channel channel, Event event);
	void setLogger(Writer out);
	Writer getLogger();
}
