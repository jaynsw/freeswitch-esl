package org.freeswitch.esl.io;

import org.freeswitch.esl.domain.ActionResponse;
import org.freeswitch.esl.domain.Command;
import org.freeswitch.esl.domain.CommandResponse;
import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.Message;
import org.freeswitch.esl.domain.SendMsg;

public interface IEngine {
	String SESSION_HANDLER_ID = "SESSION_HANDLER_ID";
	String SESSION_HANDLER_CLASS = "SESSION_HANDLER_CLASS";
	CommandResponse runCommand(long delayMilliSeconds, Command command);
	CommandResponse asyncMessage(long delayMilliSeconds, SendMsg msg);
	
	
	void addChannel(Channel channel);
	void postMessage(final Message message);
	
	

	void postEvent(final Event event);
}
