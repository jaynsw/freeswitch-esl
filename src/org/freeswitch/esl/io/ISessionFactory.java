package org.freeswitch.esl.io;

import org.freeswitch.esl.domain.Event;

public interface ISessionFactory {
	ISession newSession(IEngine engine, Channel channel, Event event);
}
