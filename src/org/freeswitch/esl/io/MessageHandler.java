package org.freeswitch.esl.io;


import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.Header.Value;
import org.freeswitch.esl.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageHandler extends  SimpleChannelInboundHandler<Message> {
	
	 protected final Logger log = LoggerFactory.getLogger( this.getClass() );
	    
	 private IEngine engine;
	 
	 public MessageHandler(IEngine engine){
		 this.engine = engine;
	 }
	    
	@Override
    protected void messageReceived(ChannelHandlerContext ctx, Message message)
            throws Exception {
       
		 
         String contentType = message.getContentType();
         if ( contentType.equals( Value.TEXT_EVENT_PLAIN ) ||
                 contentType.equals( Value.TEXT_EVENT_XML ) )
         {
             //  transform into an event
            Event eslEvent = new Event( message );
            engine.postEvent(eslEvent );
            
         }
         else
         {
             engine.postMessage(message);
         }
    }
	
	

    
        
    /**
     * Synthesise a synchronous command/response by creating a callback object which is placed in 
     * queue and blocks waiting for another IO thread to process an incoming {@link EslMessage} and
     * attach it to the callback.
     * 
     * @param channel
     * @param command single string to send
     * @return the {@link EslMessage} attached to this command's callback
     */
    
    
}
