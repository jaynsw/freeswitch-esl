package org.freeswitch.esl.io;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.ConnectException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.freeswitch.esl.domain.ActionResponse;
import org.freeswitch.esl.domain.Command;
import org.freeswitch.esl.domain.CommandResponse;
import org.freeswitch.esl.domain.Event;
import org.freeswitch.esl.domain.EventHeaderNames;
import org.freeswitch.esl.domain.Header.Value;
import org.freeswitch.esl.domain.ICommandCallback;
import org.freeswitch.esl.domain.Message;
import org.freeswitch.esl.domain.SendMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleEngine implements IEngine {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private String host = null;
	private int port = 0;
	private String password = null;

	private boolean authenticated = false;

	private io.netty.channel.Channel channel;

	private CommandResponse authenticationResponse = null;

	private ISessionFactory sessionFactory = null;

	private Map<String, Command> bgCommands = new HashMap<String, Command>();
	private Map<String, Channel> channels = new HashMap<String, Channel>();

	private final Queue<Command> commandQueue = new LinkedList<Command>();

	public static final String MESSAGE_TERMINATOR = "\n\n";
	public static final String LINE_TERMINATOR = "\n";

	Properties props;
	//private CountDownLatch authLatch;
	private ActionResponse<IEngine> startResponse = null;
	private Executor executor = Executors.newFixedThreadPool(20);

	private ScheduledExecutorService scheduledExecutor = Executors
			.newScheduledThreadPool(1);

	Bootstrap b = null;

	void init() {
		authenticated = false;
		channel = null;
		authenticationResponse = null;
	}

	public String getPassword() {
		return this.password;
	}

	public void notifyAuthResponse(CommandResponse auth) {
		authenticationResponse = auth;
		authenticated = authenticationResponse.isOk();
		startResponse.setDone(true);

		if (authenticated) {
			Command cmd = new Command(Command.NULL_API, "event", "plain ALL");
			CommandResponse response = runCommand(cmd);
			response.setCommandCallback(new ICommandCallback<CommandResponse>() {
				public void onCompleted(CommandResponse response) {
					log.debug("event plain all success...");

				}
			});
		} else {
			log.warn("authentication is failed ... ");
		}
	}


	public SimpleEngine(Properties props,
			ISessionFactory factory) {
		this.props = props;
		this.host = props.getProperty("host");
		this.port = Integer.parseInt(props.getProperty("port"));
		this.password = props.getProperty("password");
		this.sessionFactory = factory;
	}

	

	public ActionResponse<IEngine> start() {
		startResponse = new ActionResponse<IEngine>(this);
		if (canSend()) {
			stop();
		}
		init();

		EventLoopGroup workerGroup = new NioEventLoopGroup();

		b = new Bootstrap(); // (1)
		b.group(workerGroup); // (2)
		b.channel(NioSocketChannel.class); // (3)
		b.option(ChannelOption.SO_KEEPALIVE, true); // (4)
		b.handler(new ChannelInitializer<SocketChannel>() {
			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ChannelPipeline pipeline = ch.pipeline();
				pipeline.addLast("encoder", new StringEncoder());
				pipeline.addLast("decoder", new Decoder(8192));
				pipeline.addLast("handler", new MessageHandler(
						SimpleEngine.this));
			}
		});

		// Start the client.
		ChannelFuture f = b.connect(host, port); // (5)
		channel = f.channel();
		
		if (f.isDone() && !f.isSuccess()) {
			log.warn("Failed to connect to [{}:{}]", host, port);
			log.warn("  * reason: {}", f.cause());

			channel = null;

			//throw new ConnectException("Could not connect to " + host + ":" + port + ", " + f.cause());
		}

		
		return startResponse;

	}

	/**
	 * Sends a FreeSWITCH API command to the server and blocks, waiting for an
	 * immediate response from the server.
	 * <p/>
	 * The outcome of the command from the server is retured in an
	 * {@link EslMessage} object.
	 * 
	 * @param command
	 *            API command to send
	 * @param arg
	 *            command arguments
	 * @return an {@link EslMessage} containing command results
	 */
	/*
	 * public CommandResponse sendApiCommand(String command, String arg) {
	 * checkConnected(); Command cmd = new Command(Command.API, command, arg);
	 * return asyncCommand(cmd); }
	 */
	/**
	 * Submit a FreeSWITCH API command to the server to be executed in
	 * background mode. A synchronous response from the server provides a UUID
	 * to identify the job execution results. When the server has completed the
	 * job execution it fires a BACKGROUND_JOB Event with the execution results.
	 * <p/>
	 * Note that this Client must be subscribed in the normal way to
	 * BACKGOUND_JOB Events, in order to receive this event.
	 * 
	 * @param command
	 *            API command to send
	 * @param arg
	 *            command arguments
	 * @return String Job-UUID that the server will tag result event with.
	 */
	public CommandResponse sendBGApiCommand(String command, String arg) {
		checkConnected();
		Command cmd = new Command(Command.BGAPI, command, arg);
		return runCommand(cmd);
	}

	/**
	 * Set the current event subscription for this connection to the server.
	 * Examples of the events argument are:
	 * 
	 * <pre>
	 *   ALL
	 *   CHANNEL_CREATE CHANNEL_DESTROY HEARTBEAT
	 *   CUSTOM conference::maintenance
	 *   CHANNEL_CREATE CHANNEL_DESTROY CUSTOM conference::maintenance sofia::register sofia::expire
	 * </pre>
	 * 
	 * Subsequent calls to this method replaces any previous subscriptions that
	 * were set. </p> Note: current implementation can only process 'plain'
	 * events.
	 * 
	 * @param format
	 *            can be { plain | xml }
	 * @param events
	 *            { all | space separated list of events }
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse setEventSubscriptions(String format, String events) {
		// temporary hack
		if (!format.equals("plain")) {
			throw new IllegalStateException(
					"Only 'plain' event format is supported at present");
		}
		checkConnected();

		StringBuilder arg = new StringBuilder();
		if (format != null && !format.isEmpty()) {

			arg.append(format);

			if (events != null && !events.isEmpty()) {
				arg.append(' ');
				arg.append(events);
			}
		}
		Command cmd = new Command(Command.NULL_API, "event", arg.toString());
		return runCommand(cmd);
	}

	/**
	 * Cancel any existing event subscription.
	 * 
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse cancelEventSubscriptions() {
		checkConnected();
		Command cmd = new Command(Command.NULL_API, "noevents", null);
		return runCommand(cmd);
	}

	/**
	 * Add an event filter to the current set of event filters on this
	 * connection. Any of the event headers can be used as a filter. </p> Note
	 * that event filters follow 'filter-in' semantics. That is, when a filter
	 * is applied only the filtered values will be received. Multiple filters
	 * can be added to the current connection. </p> Example filters:
	 * 
	 * <pre>
	 *    eventHeader        valueToFilter
	 *    ----------------------------------
	 *    Event-Name         CHANNEL_EXECUTE
	 *    Channel-State      CS_NEW
	 * </pre>
	 * 
	 * @param eventHeader
	 *            to filter on
	 * @param valueToFilter
	 *            the value to match
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse addEventFilter(String eventHeader,
			String valueToFilter) {
		checkConnected();

		StringBuilder arg = new StringBuilder();
		if (eventHeader != null && !eventHeader.isEmpty()) {
			arg.append(eventHeader);

			if (valueToFilter != null && !valueToFilter.isEmpty()) {
				arg.append(' ');
				arg.append(valueToFilter);
			}
		}

		Command cmd = new Command(Command.NULL_API, "filter", arg.toString());
		return runCommand(cmd);
	}

	/**
	 * Delete an event filter from the current set of event filters on this
	 * connection. See {@link SimpleEngine.addEventFilter}
	 * 
	 * @param eventHeader
	 *            to remove
	 * @param valueToFilter
	 *            to remove
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse deleteEventFilter(String eventHeader,
			String valueToFilter) {
		checkConnected();
		StringBuilder arg = new StringBuilder();
		if (eventHeader != null && !eventHeader.isEmpty()) {
			arg.append("delete ");
			arg.append(eventHeader);

			if (valueToFilter != null && !valueToFilter.isEmpty()) {
				arg.append(' ');
				arg.append(valueToFilter);
			}
		}

		Command cmd = new Command(Command.NULL_API, "filter", arg.toString());
		return runCommand(cmd);
	}

	/**
	 * Send a {@link SendMsg} command to FreeSWITCH. This client requires that
	 * the {@link SendMsg} has a call UUID parameter.
	 * 
	 * @param sendMsg
	 *            a {@link SendMsg} with call UUID
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse asyncMessage(long delay, SendMsg sendMsg) {
		checkConnected();
		return asyncMultiLineCommand(delay, sendMsg.getMsgLines());

	}

	/**
	 * Enable log output.
	 * 
	 * @param level
	 *            using the same values as in console.conf
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse setLoggingLevel(String level) {
		checkConnected();
		StringBuilder arg = new StringBuilder();
		if (level != null && !level.isEmpty()) {
			arg.append(level);
		}
		Command cmd = new Command(Command.NULL_API, "log", arg.toString());
		return runCommand(cmd);
	}

	/**
	 * Disable any logging previously enabled with setLogLevel().
	 * 
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse cancelLogging() {
		checkConnected();
		Command cmd = new Command(Command.NULL_API, "log", null);
		return runCommand(cmd);
	}

	/**
	 * Close the socket connection
	 * 
	 * @return a {@link CommandResponse} with the server's response.
	 */
	public CommandResponse stop() {
		startResponse = new ActionResponse<IEngine>(this);
		checkConnected();
		Command cmd = new Command(Command.NULL_API, "exit", null);
		return runCommand(cmd);
	}

	private void checkConnected() {
		if (!canSend()) {
			throw new IllegalStateException(
					"Not connected to FreeSWITCH Event Socket");
		}
	}

	public boolean canSend() {
		return channel != null && channel.isActive() && authenticated;
	}
	
	

	public synchronized CommandResponse runCommand(Command command) {

		// command.setExe(Command.BGAPI);
		String buff = command.toString();
		log.info("Queue Command -> " + buff);
		
		commandQueue.add(command);
		
		ChannelFuture f = channel.writeAndFlush(buff);
		
			try {
				f.sync();
				if (!f.isSuccess()) {
					log.error("command {} write failed!", buff);
					commandQueue.remove(command);
				}
			} catch (InterruptedException e) {
				
			}
		
			
		
		return command.getResponse();

	}

	public CommandResponse runCommand(long milliseconds, final Command command) {
		if (milliseconds > 0) {
			scheduledExecutor.schedule(new Runnable() {
				public void run() {
					runCommand(command);
				}

			}, milliseconds, TimeUnit.MILLISECONDS);
			return command.getResponse();
		} else {
			return runCommand(command);
		}
	}

	public CommandResponse asyncMultiLineCommand(
			final long delayedMilliSeconds, final List<String> commandLines) {
		Command cmd = new Command(commandLines);
		return runCommand(delayedMilliSeconds, cmd);

	}
	
	
	

	public void handleCommandMessage(Message message) {
		Command cmd = null;
		synchronized (commandQueue) {
			cmd = commandQueue.poll();
		}
		if (cmd != null){
		CommandResponse response = cmd.getResponse();
		response.setMessage(message);
		if (response.isDone()) {
			response.complete();
		} else if (Command.BGAPI.equals(cmd.getExe()) && response.isOk()) {
			synchronized (bgCommands) {
				String jobId = response.getJobId();
				log.info("background job id: {}", jobId);
				bgCommands.put(jobId, cmd);
			}
		}
		}
	}

	protected void handleMessage(Message message) {

		String contentType = message.getContentType();
		log.info("------Received message contentType: {}------", contentType);
		log.info("message {}", message);

		if (contentType.equals(Value.API_RESPONSE)) {
			handleCommandMessage(message);
		} else if (contentType.equals(Value.COMMAND_REPLY)) {
			handleCommandMessage(message);
		} else if (contentType.equals(Value.AUTH_REQUEST)) {
			handleAuthRequest();
		} else if (contentType.equals(Value.TEXT_DISCONNECT_NOTICE)) {
			handleDisconnectionNotice();
		} else {
			log.warn("Unexpected message content type [{}]", contentType);
		}
	}

	protected void handleEvent(final Event event) {

		final String eventName = event.name;
		log.warn("------hanle event {}------", eventName);
		log.warn("event {}", event);
		String channelId = event.getChannelId();
		Channel channel = null;
		if (channelId != null) {
			log.trace("event {} triggers new channel UUID {}", eventName,
					channelId);
			synchronized (channels) {
				channel = channels.get(channelId);
			}
		}
		if ("BACKGROUND_JOB".equals(eventName)) {

			// extract the state for the corresponding FSCommand object

			String jobUUID = event.getEventHeader(EventHeaderNames.JOB_UUID);
			if (jobUUID == null) {
				log.warn("event {} does not contain Job-UUID");
			} else {

				// remove extracted command from list of active commands
				Command cmd = null;
				synchronized (bgCommands) {
					cmd = bgCommands.remove(jobUUID);
				}
				if (cmd == null) {
					log.warn("did not find active command UUID {}", jobUUID);
				} else {

					log.trace("BGAPI completed with job UUID {}", jobUUID);

					CommandResponse response = cmd.getResponse();
					String replyText = null;
					if (event.getEventBodyLines().size() > 0){
						replyText = event.getEventBodyLines().get(0);
						log.trace("BGAPI completed with replyText {}", replyText);
					}
					
					response.setReplyText(replyText);
					response.setDone(true);
					response.complete();
				}
			}
		} else if ("CHANNEL_UUID".equals(eventName)) {
			if (channelId != null) {
				String oldChannelId = event
						.getEventHeader(EventHeaderNames.OLD_UNIQUE_ID);
				synchronized (channels) {
					channel = channels.get(oldChannelId);
				}
				log.trace("event {} triggers channel UUID {} to get active",
						eventName, oldChannelId);
				if (channel != null) {
					channel.setChannelId(channelId);
					synchronized (channels) {
						channels.remove(oldChannelId);
						channels.put(channelId, channel);
					}
				} else {
					log.trace("event {} cannot find new UUID {}", eventName,
							oldChannelId);
				}

			} else {
				log.warn("event {} channelId is null {}", eventName);
			}
		} else if ("CHANNEL_CREATE".equals(eventName)) {
			if (channelId != null && channel == null) {
				String callDirection = event
						.getEventHeader(EventHeaderNames.CALL_DIRECTION);
				String callerId = event
						.getEventHeader(EventHeaderNames.CALLER_CALLER_ID_NUMBER);
				String DID = event
						.getEventHeader(EventHeaderNames.CALLER_DESTINATION_NUMBER);
				
					
				
				channel = new Channel(channelId, SimpleEngine.this,
						callDirection);
				channel.setState(Channel.State.ACTIVE);
				ISession session = sessionFactory.newSession(this, channel, event);
				channel.setSession(session);
				channel.setFrom(callerId);
				channel.setTo(DID);
				Writer fout = channel.getSession().getLogger();
				if (fout == null){
					Calendar cal = Calendar.getInstance();
					String dirP = props.getProperty("log_root") + "/" + cal.get(Calendar.YEAR) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.HOUR_OF_DAY);
					File dir = new File(dirP);
					dir.mkdirs();
					String path = dirP + "/" + channelId + ".txt"; 
					
						
							try {
								fout = new OutputStreamWriter(new FileOutputStream(new File(path)),"UTF-8");
							} catch (UnsupportedEncodingException
									| FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
					channel.getSession().setLogger(fout);
				}
				synchronized (channels) {
					channels.put(channelId, channel);
				}
			}
			
			log.info("new object channel {}", channelId);
		} else if ("CHANNEL_DESTROY".equals(eventName)) {
			if (channelId != null) {
				synchronized (channels) {
					channel = channels.remove(channelId);
				}
				if (channel != null){
					channel.setState(Channel.State.DEAD);
				}
			} else {
				log.warn("event {} without channel UUID", event);
			}
		}

		if (channel != null) {
			final Channel ch = channel;
			executor.execute(new Runnable() {
				public void run() {
					Writer out = ch.getSession().getLogger();
					if (out != null){
						try {
							out.write(event.toString() + "\n\n");
							out.flush();
							if ("CHANNEL_DESTROY".equals(eventName)){
								out.close();
								ch.getSession().setLogger(null);
							}
							
						} catch (IOException e) {
							log.error(e.getMessage(), e);
						}
						
						
						
					}
					synchronized(ch){
						ch.onEvent(event);
					}
				}
			});

		} else {
			log.info("event without channe id");
		}

	}

	protected void handleAuthRequest() {

		log.debug("Auth requested, sending [auth {}]", "*****");
		Command cmd = new Command(Command.NULL_API, "auth", password);

		CommandResponse response = runCommand(cmd);
		response.setCommandCallback(new ICommandCallback<CommandResponse>() {
			public void onCompleted(CommandResponse response) {
				log.debug("Auth response [{}]", response);

				log.debug("Auth response success={}, message=[{}]",
						response.isOk(), response.getReplyText());
				notifyAuthResponse(response);
			}
		});
	}

	protected void handleDisconnectionNotice() {
	}

	public void postMessage(final Message message) {
		handleMessage(message);
	}

	public void postEvent(final Event event) {
		handleEvent(event);
	}

	public void addChannel(Channel channel) {
		synchronized (channels) {
			channels.put(channel.getChannelId(), channel);
		}
	}
}
