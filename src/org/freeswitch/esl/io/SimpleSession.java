package org.freeswitch.esl.io;

import java.io.Writer;

import org.freeswitch.esl.domain.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSession extends SessionAdapter implements ISession {

	private static final Logger logger = LoggerFactory
			.getLogger(SimpleSession.class);

	String sessionId;

	IEngine engine;
	
	Writer writer;

	public SimpleSession(IEngine engine) {
		this.engine = engine;
	}

	
	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public void setSessionId(String id) {
		this.sessionId = id;
	}

	public IEngine getEngine() {
		return engine;
	}

	@Override
	public void onEvent(Channel channel, Event event) {

		logger.info("---Session onEvent channl {} event {}",
				channel.getChannelId(), event.getEventName());
		String eventName = event.name;
		if ("CHANNEL_APPLICATION".equals(eventName)) {
			this.onEventChannelApplication(channel, event);
		} else if ("PRIVATE_COMMAND".equals(eventName)) {
			this.onEventPrivateCommand(channel, event);
		} else if ("PLAYBACK_START".equals(eventName)) {
			this.onEventPlaybackStart(channel, event);
		} else if ("PLAYBACK_STOP".equals(eventName)) {
			this.onEventPlaybackStop(channel, event);
		} else if ("DTMF".equals(eventName)) {
			this.onEventDTMF(channel, event);
		} else if ("RECORD_START".equals(eventName)) {
			this.onEventRecordStart(channel, event);
		} else if ("RECORD_STOP".equals(eventName)) {
			this.onEventRecordStop(channel, event);
		} else if ("CHANNEL_ANSWER".equals(eventName)) {
			this.onEventChannelAnswer(channel, event);
		} else if ("CHANNEL_BRIDGE".equals(eventName)) {
			this.onEventChannelBridge(channel, event);
		} else if ("CHANNEL_CALLSTATE".equals(eventName)) {
			this.onEventChannelCallState(channel, event);
		} else if ("CHANNEL_CREATE".equals(eventName)) {
			this.onEventChannelCreate(channel, event);
		} else if ("CHANNEL_DATA".equals(eventName)) {
			this.onEventChannelData(channel, event);
		} else if ("CHANNEL_DESTROY".equals(eventName)) {
			this.onEventChannelDestroy(channel, event);
		} else if ("CHANNEL_EXECUTE".equals(eventName)) {
			this.onEventChannelExecute(channel, event);
		} else if ("CHANNEL_EXECUTE_COMPLETE".equals(eventName)) {
			this.onEventChannelExecuteComplete(channel, event);
		} else if ("CHANNEL_HANGUP".equals(eventName)) {
			this.onEventChannelHangup(channel, event);
		} else if ("CHANNEL_HANGUP_COMPLETE".equals(eventName)) {
			this.onEventChannelHangupComplete(channel, event);
		} else if ("CHANNEL_HOLD".equals(eventName)) {
			this.onEventChannelHold(channel, event);
		} else if ("CHANNEL_ORIGINATE".equals(eventName)) {
			this.onEventChannelOriginate(channel, event);
		} else if ("CHANNEL_OUTGOING".equals(eventName)) {
			this.onEventChannelOutgoing(channel, event);
		} else if ("CHANNEL_PARK".equals(eventName)) {
			this.onEventChannelPark(channel, event);
		} else if ("CHANNEL_PROGRESS".equals(eventName)) {
			this.onEventChannelProgress(channel, event);
		} else if ("CHANNEL_PROGRESS_MEDIA".equals(eventName)) {
			this.onEventChannelProgressMedia(channel, event);
		} else if ("CHANNEL_STATE".equals(eventName)) {
			this.onEventChannelState(channel, event);
		} else if ("CHANNEL_UNBRIDGE".equals(eventName)) {
			this.onEventChannelUnBridge(channel, event);
			;
		} else if ("CHANNEL_UNHOLD".equals(eventName)) {
			this.onEventChannelUnHold(channel, event);
		} else if ("CHANNEL_UNPARK".equals(eventName)) {
			this.onEventChannelUnPark(channel, event);
		} else if ("CHANNEL_UUID".equals(eventName)) {
			this.onEventChannelUUID(channel, event);
		} else {
			logger.info("event {} not handled", eventName);
		}

	}


	@Override
	public void setLogger(Writer out) {
		this.writer = out;
	}


	@Override
	public Writer getLogger() {
		return writer;
	}
}
