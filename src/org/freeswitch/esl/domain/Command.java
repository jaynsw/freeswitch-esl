package org.freeswitch.esl.domain;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Command {
	private static final Logger log = LoggerFactory.getLogger( Command.class );
	
	String exe;
	String command;
    String args;
    
    
    public final static String SINGLE_LINE = "single";
    public final static String MULTI_LINE = "multi";
    
    //public final static String API = "api ";
    public final static String BGAPI = "bgapi ";
    public final static String NULL_API = "";
    
    public static final String MESSAGE_TERMINATOR = "\n\n";  
	public static final String LINE_TERMINATOR = "\n";
	 
	 
    UUID uuid;
   
   
    private String contentType = SINGLE_LINE;
    
    private List<String> commandLines = null;
    
    private CommandResponse response;
    /**
     * Creates a FSCommand object (API call)
     * 
     * @param command
     *            the Freeswitch API command, e.g. "originate"
     * @param args
     *            the command arguments
     * @param callback
     *            a callback object whose onSuccess() or onError() 
     *            methods are called when the command completes 
     */
    public Command(String exe, String command, String args) {
        uuid = null;
        this.exe = exe;
        this.command = command;
        this.args = args;
        this.response = new CommandResponse(this);
    }

    
    public Command(final List<String> commandLines){
    	this.commandLines = commandLines;
    	this.contentType = MULTI_LINE;
    	this.response = new CommandResponse(this);
    }

    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUUID() {
        return uuid;
    }

    
    
    
    
    public String getExe(){
    	return exe;
    }
    

    @Override
    public String toString() {
    	if (SINGLE_LINE.equals(contentType)) {
    		return exe + command + " " + args + MESSAGE_TERMINATOR;
    	} else {
    		StringBuilder sb = new StringBuilder();
            for ( String line : commandLines )
            {
                sb.append( line );
                sb.append( LINE_TERMINATOR );
            }
            sb.append( LINE_TERMINATOR );
            
            return sb.toString();
    	}
    }
    
    
    
    
    
    public void setExe(String exe){
    	this.exe = exe;
    }
    
    public CommandResponse getResponse(){
    	return response;
    }
}
