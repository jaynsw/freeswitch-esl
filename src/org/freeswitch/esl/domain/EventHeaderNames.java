package org.freeswitch.esl.domain;

public class EventHeaderNames {
    /**
     * {@code "Event-Name"}
     */
    public static final String EVENT_NAME = "Event-Name";
    
    public static final String EVENT_SUBCLASS = "Event-Subclass";
    
    public static final String UNIQUE_ID = "Unique-ID";
    /**
     * {@code "Event-Date-Local"}
     */
    public static final String EVENT_DATE_LOCAL = "Event-Date-Local";
    /**
     * {@code "Event-Date-GMT"}
     */
    public static final String EVENT_DATE_GMT = "Event-Date-GMT";
    /**
     * {@code "Event-Date-Timestamp"}
     */
    public static final String EVENT_DATE_TIMESTAMP = "Event-Date-Timestamp";
    /**
     * {@code "Event-Calling-File"}
     */
    public static final String EVENT_CALLING_FILE = "Event-Calling-File";
    /**
     * {@code "Event-Calling-Function"}
     */
    public static final String EVENT_CALLING_FUNCTION = "Event-Calling-Function";
    /**
     * {@code "Event-Calling-Line-Number"}
     */
    public static final String EVENT_CALLING_LINE_NUMBER = "Event-Calling-Line-Number";
    /**
     * {@code "FreeSWITCH-Hostname"}
     */
    public static final String FREESWITCH_HOSTNAME = "FreeSWITCH-Hostname";
    /**
     * {@code "FreeSWITCH-IPv4"}
     */
    public static final String FREESWITCH_IPV4 = "FreeSWITCH-IPv4";
    /**
     * {@code "FreeSWITCH-IPv6"}
     */
    public static final String FREESWITCH_IPV6 = "FreeSWITCH-IPv6";
    /**
     * {@code "Core-UUID"}
     */
    public static final String CORE_UUID = "Core-UUID";
    /**
     * {@code "Content-Length"}
     */
    public static final String CONTENT_LENGTH = "Content-Length";
    /**
     * {@code "Job-Command"}
     */
    public static final String JOB_COMMAND = "Job-Command";
    /**
     * {@code "Job-UUID"}
     */
    public static final String JOB_UUID = "Job-UUID";
    
    public static final String OLD_UNIQUE_ID = "Old-Unique-ID";
    
    public static final String CALL_DIRECTION = "Caller-Direction";
    
    public static final String CHANNEL_STATE = "Channel-State";
    
    public static final String APPLICATION = "Application";
    public static final String APPLICATION_DATA = "Application-Data";
    public static final String APPLICATION_RESPONSE = "Application-Response";
    public static final String CALLER_DESTINATION_NUMBER = "Caller-Destination-Number";
    
    public static final String DTMF_DIGIT = "DTMF-Digit";
    
    public static final String PLAYBACK_RESPONSE_FILE_NOT_FOUND = "FILE NOT FOUND";
    public static final String PLAYBACK_RESPONSE_FILE_PLAYED = "FILE PLAYED";
    public static final String PLAYBACK_RESPONSE_PLAYBACK_ERROR = "PLAYBACK ERROR";
    
    public static final String CALLER_CALLER_ID_NUMBER = "Caller-Caller-ID-Number ";
    
    public static final String HANGUP_CAUSE = "Hangup-Cause";

    private EventHeaderNames()
    {
        /* private class */
    }
}
