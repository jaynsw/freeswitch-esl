package org.freeswitch.esl.domain;

import java.util.concurrent.CountDownLatch;

import org.freeswitch.esl.domain.Header.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CommandResponse {

	private static final Logger log = LoggerFactory.getLogger( CommandResponse.class );
	
	
	    private  Command command;
	    private String replyText;
	    private Message response;
	    private boolean success;
	    
	    private ICommandCallback<CommandResponse> callback;
	    
	    private String jobId;
	    
	    private boolean isDone = false;
	    
	    
	    private final CountDownLatch latch = new CountDownLatch( 1 );
	    
	    public CommandResponse(Command command){
	    	this.command = command;
	    }
	    public void setCommandCallback(ICommandCallback<CommandResponse> callback){
	    	this.callback = callback;
	    	if (isDone()){
	    		callback.onCompleted(this);
	    	}
	    }
	    
	    public ICommandCallback<CommandResponse> getCommandCallback(){
	    	return callback;
	    }
	    
	    public CommandResponse get(){
	    	try
	        {
	            log.trace( "awaiting latch ... " );
	            latch.await();
	        }
	        catch ( InterruptedException e )
	        {
	            throw new RuntimeException( e );
	        }
	    	return this;
	    }
	    
	    public void complete(){
	    	if (isDone) {
	    	latch.countDown();
	        if (callback != null){
	        	callback.onCompleted(this);
	        }
	    	}
	    }
	    
	    public boolean setMessage(Message response){
	    	
	    	this.response = response;
	    	this.replyText = response.getHeaderValue( Header.Name.REPLY_TEXT.literal() );
	        this.success = replyText.startsWith( "+OK" );
	        
	        isDone = false;
	        
	        String exe = command.getExe();
        	if (Command.BGAPI.equals(exe)){
        		if (success){
	        	
	        		//Reply-Text: +OK Job-UUID: c7709e9c-1517-11dc-842a-d3a3942d3d63
	        		String jjobId = replyText.substring("+OK Job-UUID:".length()).trim();
	        		jobId = jjobId.trim();
	        	} else {
	        		isDone = true;
	        	}
	        } else {
	        	isDone = true;
	        }
	        
        	return isDone;
	        
	        
	        
	    }
	    
	    public boolean isDone(){
	    	return isDone;
	    }
	 
	    /**
	     * @return the original command sent to the server
	     */
	    public Command getCommand()
	    {
	        return command;
	    }
	    
	    /**
	     * @return true if and only if the response Reply-Text line starts with "+OK"
	     */
	    public boolean isOk()
	    {
	        return success;
	    }
	    
	    /**
	     * @return the full response Reply-Text line.
	     */
	    public String getReplyText()
	    {
	        return replyText;
	    }
	    
	    /**
	     * @return {@link EslMessage} the full response from the server 
	     */
	    public Message getResponse()
	    {
	        return response;
	    }
	    
	    public String getJobId(){
	    	return jobId;
	    }
	    
	    public void setReplyText(String val){
	    	this.replyText = val;
	    	this.success = replyText.startsWith( "+OK" );
		}
	    
	    public void setDone(boolean val){
	    	this.isDone = val;
	    }
	}
