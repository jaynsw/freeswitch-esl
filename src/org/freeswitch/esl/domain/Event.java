package org.freeswitch.esl.domain;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.freeswitch.esl.domain.Header.Value;
import org.freeswitch.esl.io.HeaderParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Event {
	 private final Logger log = LoggerFactory.getLogger( this.getClass() );
	 
	 
	    
	 	public String name;
	    public String channelId;
	    Message message;
	    
	    private final Map<String,String> messageHeaders;
	    private final Map<String,String> eventHeaders;
	    private final List<String> eventBody;
	    private boolean decodeEventHeaders = true;
	    
	    public Event( Message rawMessage )
	    {
	        this( rawMessage, false );
	    }
	    
	    public Event( Message rawMessage, boolean parseCommandReply )
	    {
	    	this.message = rawMessage;
	        messageHeaders = rawMessage.getHeaders();
	        eventHeaders = new HashMap<String,String>( rawMessage.getBodyLines().size() );
	        eventBody = new ArrayList<String>();
	        // plain or xml body
	        if ( rawMessage.getContentType().equals( Value.TEXT_EVENT_PLAIN ) )
	        {
	            parsePlainBody( rawMessage.getBodyLines() );
	        }
	        else if ( rawMessage.getContentType().equals( Value.TEXT_EVENT_XML ) )
	        {
	            throw new IllegalStateException( "XML events are not yet supported" );
	        }
	        else if ( rawMessage.getContentType().equals( Value.COMMAND_REPLY ) && parseCommandReply )
	        {
	            parsePlainBody( rawMessage.getBodyLines() );
	        }
	        else
	        {
	            throw new IllegalStateException( "Unexpected EVENT content-type: " + 
	                rawMessage.getContentType() );
	        }
	    }

	    /**
	     * The message headers of the original ESL message from which this event was decoded.
	     * The message headers are stored in a map keyed by {@link EslHeaders.Name}. The string mapped value 
	     * is the parsed content of the header line (ie, it does not include the header name).
	     *  
	     * @return map of header values
	     */
	    public Map<String,String> getMessageHeaders()
	    {
	        return messageHeaders;
	    }

	    /**
	     * The event headers of this event. The headers are parsed and stored in a map keyed by the string 
	     * name of the header, and the string mapped value is the parsed content of the event header line 
	     * (ie, it does not include the header name).
	     *  
	     * @return map of event header values
	     */
	    public Map<String, String> getEventHeaders()
	    {
	        return eventHeaders;
	    }
	    
	    public String getEventHeader(String name){
	    	return eventHeaders.get(name);
	    }
	    
	    /**
	     * Any event body lines that were present in the event.
	     * 
	     * @return list of decoded event body lines, may be an empty list.
	     */
	    public List<String> getEventBodyLines()
	    {
	        return eventBody;
	    }
	    
	    /**
	     * Convenience method.
	     * 
	     * @return the string value of the event header "Event-Name"
	     */
	    public String getEventName()
	    {
	        return getEventHeaders().get( EventHeaderNames.EVENT_NAME); 
	    }
	    
	    /**
	     * Convenience method.
	     * 
	     * @return long value of the event header "Event-Date-Timestamp"
	     */
	    public long getEventDateTimestamp()
	    {
	        return Long.valueOf( getEventHeaders().get( EventHeaderNames.EVENT_DATE_TIMESTAMP ) );
	    }

	    /**
	     * Convenience method.
	     * 
	     * @return long value of the event header "Event-Date-Local"
	     */
	    public String getEventDateLocal()
	    {
	        return getEventHeaders().get( EventHeaderNames.EVENT_DATE_LOCAL );
	    }

	    /**
	     * Convenience method.
	     * 
	     * @return long value of the event header "Event-Date-GMT"
	     */
	    public String getEventDateGmt()
	    {
	        return getEventHeaders().get( EventHeaderNames.EVENT_DATE_GMT );
	    }

	    /**
	     * Convenience method.
	     * 
	     * @return true if the eventBody list is not empty.
	     */
	    public boolean hasEventBody()
	    {
	        return ! eventBody.isEmpty();
	    }
	    
	    public String getChannelId(){
	    	return channelId;
	    }
	    
	    private void parsePlainBody( final List<String> rawBodyLines )
	    {
	        boolean isEventBody = false;
	        for ( String rawLine : rawBodyLines )
	        {
	            if ( ! isEventBody )
	            {
	                // split the line
	                String[] headerParts = HeaderParser.splitHeader( rawLine );
	                if ( decodeEventHeaders )
	                {
	                    try
	                    {
	                        String decodedValue = URLDecoder.decode( headerParts[1], "UTF-8" );
	                        log.trace( "decoded from: [{}]", headerParts[1] );
	                        log.trace( "decoded   to: [{}]", decodedValue );
	                        eventHeaders.put( headerParts[0], decodedValue );
	                    }
	                    catch ( UnsupportedEncodingException e )
	                    {
	                        log.warn( "Could not URL decode [{}]", headerParts[1] );
	                        eventHeaders.put( headerParts[0], headerParts[1] );
	                    }
	                }
	                else
	                {
	                    eventHeaders.put( headerParts[0], headerParts[1] );
	                }
	                if ( headerParts[0].equals( EventHeaderNames.CONTENT_LENGTH ) )
	                {
	                    // the remaining lines will be considered body lines
	                    isEventBody = true;
	                }
	            }
	            else
	            {
	                // ignore blank line (always is one following the content-length
	                if ( rawLine.length() > 0 )
	                {
	                    eventBody.add( rawLine );
	                }
	            }
	        }
	     // extract the event name
	        String eventName = eventHeaders.get(EventHeaderNames.EVENT_NAME);
	        if (eventName != null) {
	            // CUSTOM events require special treatment
	            if (eventName.equals("CUSTOM")) {
	                String eventSubclass = eventHeaders.get(EventHeaderNames.EVENT_SUBCLASS);
	                // construct the event name of CUSTOM and subclass
	                if (eventSubclass != null)
	                    eventName = "CUSTOM_" + eventSubclass.toUpperCase().replaceAll("::", "_");
	            }
	            // now the event should be one of our pre-defined enum values
	            name = eventName;
	        } else {
	            // no Event-Name, that shouldn't happen, but who knows...
	            name ="UNKNOWN-Event-Name";
	            log.warn("Freeswitch event without event-name attribute");
	        }
	        
	        channelId = eventHeaders.get(EventHeaderNames.UNIQUE_ID);
	        
	    }
	    
	    
	    public Message getMessage(){
	    	return this.message;
	    }
	    
	    @Override
	    public String toString()
	    {
	        StringBuilder sb = new StringBuilder( "EslEvent: name=[" );
	        sb.append( getEventName() );
	        sb.append( "] headers=" );
	        sb.append( messageHeaders.size() );
	        sb.append( ", eventHeaders=" );
	        sb.append( eventHeaders.size() );
	        sb.append( ", eventBody=" );
	        sb.append( eventBody.size() );
	        sb.append( " lines.\n" );
	        
	        sb.append("---headers---\n");
	        for (Map.Entry<String,String> entry : eventHeaders.entrySet()){
	        	sb.append(entry.getKey() + " : " + entry.getValue() + "\n");
	        }
	        sb.append("---body---");
	        for (String ln : eventBody){
	        	sb.append(ln  + "\n");
	        }
	        
	        return sb.toString();
	    }
	    
	    public boolean isBackgroundJobOK(){
	    	
	    	if (name != null && "BACKGROUND_JOB".equals(name) && eventBody.size() > 0){
	    		String line = eventBody.get(0);
	    		return line.startsWith("+OK");
	    	}
	    	return false;
	    }
	    
	    public String getVariable(String key){
	    	return eventHeaders.get("variable_" + key);
	    }
	}
