package org.freeswitch.esl.domain;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ActionResponse<T> {

	private static final Logger log = LoggerFactory.getLogger( ActionResponse.class );
	
	
	    private T t;
	   
	    private boolean isDone = false;
	    private ICommandCallback<T> callback;
	    
	    
	    private final CountDownLatch latch = new CountDownLatch( 1 );
	    
	    public ActionResponse(T t){
	    	this.t = t;
	    }
	    public synchronized void setCommandCallback(ICommandCallback<T> callback){
	    	this.callback = callback;
	    	if (isDone){
	    		callback.onCompleted(t);
	    	}
	    }
	    
	    public ICommandCallback<T> getCommandCallback(){
	    	return callback;
	    }
	    
	    public T get(){
	    	try
	        {
	            log.trace( "awaiting latch ... " );
	            latch.await();
	        }
	        catch ( InterruptedException e )
	        {
	            throw new RuntimeException( e );
	        }
	    	return t;
	    }
	    
	   
	    
	    public synchronized boolean setDone(boolean isDone){
	    	
	    
	        this.isDone = isDone;
	    	if (isDone) {
	    		latch.countDown();
	    		if (callback != null){
	    			callback.onCompleted(t);
	    		}
	    	}
	        return isDone;
	    }
	    
	   
	 
	    
	    public boolean isDone(){
	    	return isDone;
	    }
	    	    
	    
	}
