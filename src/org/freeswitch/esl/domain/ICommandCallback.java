package org.freeswitch.esl.domain;

public interface ICommandCallback<Response> {
    
    void onCompleted(Response response);

}
