package org.freeswitch.esl.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.freeswitch.esl.domain.Header.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Message {
	 private final Logger log = LoggerFactory.getLogger( this.getClass() );
	    
	    private final Map<String,String> headers = new HashMap<String,String>();
	    private final List<String> body = new ArrayList<String>();
	    
	    private Integer contentLength = null;

	    /**
	     * All the received message headers in a map keyed by {@link EslHeaders.Name}. The string mapped value 
	     * is the parsed content of the header line (ie, it does not include the header name).
	     *  
	     * @return map of header values
	     */
	    public Map<String,String> getHeaders()
	    {
	        return headers;
	    }

	    /**
	     * Convenience method
	     * 
	     * @param headerName as a {@link EslHeaders.Name}
	     * @return true if an only if there is a header entry with the supplied header name 
	     */
	    public boolean hasHeader( String headerName )
	    {
	        return headers.containsKey( headerName );
	    }

	    /**
	     * Convenience method
	     * 
	     * @param headerName as a {@link EslHeaders.Name}
	     * @return same as getHeaders().get( headerName )
	     */
	    public String getHeaderValue( String headerName )
	    {
	        return headers.get( headerName );
	    }
	    
	    /**
	     * Convenience method
	     * 
	     * @return true if and only if a header exists with name "Content-Length" 
	     */
	    public boolean hasContentLength()
	    {
	        return headers.containsKey( Name.CONTENT_LENGTH.literal() );
	    }
	    
	    /**
	     * Convenience method
	     * 
	     * @return integer value of header with name "Content-Length"
	     */
	    public Integer getContentLength()
	    {
	        if ( contentLength != null )
	        {
	            return contentLength;
	        }
	        if ( hasContentLength() )
	        {
	            contentLength = Integer.valueOf( headers.get( Name.CONTENT_LENGTH.literal()) );
	        }
	        return contentLength;
	    }
	    
	    /**
	     * Convenience method
	     * 
	     * @return header value of header with name "Content-Type"
	     */
	    public String getContentType()
	    {
	        return headers.get( Name.CONTENT_TYPE.literal() );
	    }

	    /**
	     * Any received message body lines
	     * 
	     * @return list with a string for each line received, may be an empty list
	     */
	    public List<String> getBodyLines()
	    {
	        return body;
	    }
	    
	    /**
	     * Used by the {@link EslMessageDecoder}.
	     * 
	     * @param name
	     * @param value
	     */
	    public void addHeader( String name, String value )
	    {
	        log.debug( "adding header [{}] [{}]", name, value );
	        headers.put( name, value );
	    }
	    
	    /**
	     * Used by the {@link EslMessageDecoder}
	     * 
	     * @param line
	     */
	    public void addBodyLine( String line )
	    {
	        if ( line == null )
	        {
	            return;
	        }
	        body.add( line );
	    }

	    @Override
	    public String toString()
	    {
	        StringBuilder sb = new StringBuilder( "EslMessage: contentType=[" );
	        sb.append( getContentType() );
	        sb.append( "] headers=" );
	        sb.append( headers.size() );
	        sb.append( ", body=" );
	        sb.append( body.size() );
	        sb.append( " lines.\n" );
	        sb.append("---headers---\n");
	        for (Map.Entry<String,String> entry : headers.entrySet()){
	        	sb.append(entry.getKey() + " : " + entry.getValue()  + "\n");
	        }
	        sb.append("---body---\n");
	        for (String ln : body){
	        	sb.append(ln + "\n");
	        }
	        
	        return sb.toString();
	    }
}
